﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MAS.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bikes",
                columns: table => new
                {
                    BikeId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    AdditionalInfo = table.Column<string>(nullable: true),
                    TeethStr = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bikes", x => x.BikeId);
                });

            migrationBuilder.CreateTable(
                name: "Parts",
                columns: table => new
                {
                    PartId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    ProducerName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parts", x => x.PartId);
                });

            migrationBuilder.CreateTable(
                name: "BikeAges",
                columns: table => new
                {
                    BikeAgeId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BikeId = table.Column<int>(nullable: false),
                    ProductionYear = table.Column<int>(nullable: false),
                    DesignYear = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BikeAges", x => x.BikeAgeId);
                    table.ForeignKey(
                        name: "FK_BikeAges_Bikes_BikeId",
                        column: x => x.BikeId,
                        principalTable: "Bikes",
                        principalColumn: "BikeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BikeServices",
                columns: table => new
                {
                    ServiceId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Date = table.Column<DateTime>(nullable: false),
                    BikeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BikeServices", x => x.ServiceId);
                    table.ForeignKey(
                        name: "FK_BikeServices_Bikes_BikeId",
                        column: x => x.BikeId,
                        principalTable: "Bikes",
                        principalColumn: "BikeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BikeSetups",
                columns: table => new
                {
                    PartId = table.Column<int>(nullable: false),
                    BikeId = table.Column<int>(nullable: false),
                    SetupName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BikeSetups", x => new { x.BikeId, x.PartId });
                    table.ForeignKey(
                        name: "FK_BikeSetups_Bikes_BikeId",
                        column: x => x.BikeId,
                        principalTable: "Bikes",
                        principalColumn: "BikeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BikeSetups_Parts_PartId",
                        column: x => x.PartId,
                        principalTable: "Parts",
                        principalColumn: "PartId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Diagnosis",
                columns: table => new
                {
                    DiagnosisId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Grade = table.Column<int>(nullable: false),
                    ServiceId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Diagnosis = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Diagnosis", x => x.DiagnosisId);
                    table.ForeignKey(
                        name: "FK_Diagnosis_BikeServices_Diagnosis",
                        column: x => x.Diagnosis,
                        principalTable: "BikeServices",
                        principalColumn: "ServiceId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BikeAges_BikeId",
                table: "BikeAges",
                column: "BikeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BikeServices_BikeId",
                table: "BikeServices",
                column: "BikeId");

            migrationBuilder.CreateIndex(
                name: "IX_BikeSetups_PartId",
                table: "BikeSetups",
                column: "PartId");

            migrationBuilder.CreateIndex(
                name: "IX_Diagnosis_Diagnosis",
                table: "Diagnosis",
                column: "Diagnosis");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BikeAges");

            migrationBuilder.DropTable(
                name: "BikeSetups");

            migrationBuilder.DropTable(
                name: "Diagnosis");

            migrationBuilder.DropTable(
                name: "Parts");

            migrationBuilder.DropTable(
                name: "BikeServices");

            migrationBuilder.DropTable(
                name: "Bikes");
        }
    }
}

# [MAS] Described requirements for MP 01

**class extent** - collection of all objects for a given class

**class extent - persistence** - when we close the program and run it again, we have the same data after we launch it again (for example serialization, object stream, database...).
All fields in your class should also implement Serializable. If we don't do that, we get an exception. Keyword _transient_ means that a field won't be serialized.

**complex attribute** - attribute which can have many fields (for example Person has attribute Address, and then we have additional fields like city, street etc.).

**optional attribute** - [0..1] (cardinality from 0 to 1), appropriate o type is still required (must have a possibility to be null).

**multi-value attribute** - attribute with many values (e.g. collection), [0..*], [1..*] etc. cardinality. We should make add/remove methods which will check lower/upper bounds.

**class attribute** - static attribute basicly. E.g. minimum age.

**derrived attribute** - attribute (will be get[DerivedAttributeName] method) which will calculate the value basing on other attributes. _Should not be a field in this mini project!_

**class method** - static method.

**method overriding and overloading** - no need for explainations.

## Some additional a bit important info

* Don't return direct reference to a collection! Return a reference to a copy of an Array, ArrayList etc. We can also use a wrapper - Collections.unmodifiableList(...) in Java.
* In Java we have to use object types if there is a possbility, that a value will be null (woah).


# [MAS] Described requirements for MP 02

**basic association**

**association with attribute**

**qualified association** - qualifier is a unique attribute, maps are good for connecting two objects. Qualifier (for example "name") is an attribute. In second class there should be a mapp which keeps objects from "many" side. Object on "one" side should get a qualifier from object (by a getter).

**composition** - we have more constraints than as in normal association. Inner class is enough.

Association means UNIQUE connections.

Connection should have 3 methods:
- addSth
- removeSth
- getSth
Methods should be also on the second side of an association.

Better not use setter.


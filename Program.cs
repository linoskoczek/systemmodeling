﻿using System;
using System.Collections.Generic;
using System.Linq;
using MAS.Models;
using Microsoft.EntityFrameworkCore;

namespace MAS {
    class Program {
        private static BikeContext _context = new BikeContext();

        static void Main(string[] args) {
            seed();

            var Bikes = _context.Bikes.ToList();
            foreach (Bike bike in Bikes) {
                bike.LoadBikeData();
                Console.WriteLine(bike);
            }
            var exampleBike = Bikes.First(b => b.Name == "Dupowóz2001");

            //MP1Examples(exampleBike);
            MP2Examples(exampleBike);
        }

        private static void MP2Examples(Bike exampleBike) {
            Console.WriteLine("\n==Basic association==");
            Console.WriteLine(exampleBike.Name + "\n Id:" +
                exampleBike.BikeAge.BikeId + "\n Age: " +
                exampleBike.BikeAge.Age);

            Console.WriteLine("\n==Association with attribute==");
            foreach (var setup in exampleBike.BikeSetups.ToList()) {
                Console.WriteLine(setup.Bike.Name + " : " + setup.Part.Name);
            }

            Console.WriteLine("\n==Qualified association==");
            foreach (var setup in exampleBike.BikeSetups.ToList()) {
                var producer = setup.Part.ProducerName;
                Console.WriteLine(setup.Part.Name + " from " +
                    Producer.Producers.GetValueOrDefault(setup.Part.ProducerName).Headquarters);
            }

            Console.WriteLine("\n==Composition==");
            foreach (var service in exampleBike.Services.ToList()) {
                Console.WriteLine(service.Bike.Name + " : ");
                foreach (var diag in service.GetDiagnosis()) {
                    Console.WriteLine("- " + diag.Description + " -> " + diag.Grade);
                }
            }
            Console.WriteLine();
        }

        static void seed() {
            _context.Database.ExecuteSqlCommand("DELETE FROM BikeSetups");
            _context.Database.ExecuteSqlCommand("DELETE FROM Bikes");
            _context.Database.ExecuteSqlCommand("DELETE FROM Parts");
            _context.Database.ExecuteSqlCommand("DELETE FROM Diagnosis");
            _context.Database.ExecuteSqlCommand("DELETE FROM BikeServices");
            _context.Database.ExecuteSqlCommand("DELETE FROM BikeAges");
            _context.Database.ExecuteSqlCommand("VACUUM");

            new Producer("Shimano", "Warsaw");
            new Producer("Cube", "Wroclaw");
            new Producer("Magura", "Wroclaw");
            new Producer("Dartmoor", "Gdansk");

            var brakes1 = new Part("Brakes", "Shimano");
            var brakes2 = new Part("Brakes", "Magura");
            var frame1 = new Part("Frame", "Cube");
            var frame2 = new Part("Frame", "Dartmoor");
            var simplyBike = new Bike("Dupowóz3000");
            var bikeBetter = new Bike("Dupowóz2001");

            bikeBetter.AddTeeth("10y");
            bikeBetter.AddTeeth("16y");
            bikeBetter.AddTeeth("22b");
            bikeBetter.RemoveTeeth("16b");

            _context.Parts.Add(brakes1);
            _context.Parts.Add(brakes2);
            _context.Parts.Add(frame1);
            _context.Parts.Add(frame2);
            _context.Bikes.Add(simplyBike);
            _context.Bikes.Add(bikeBetter);
            _context.SaveChanges();

            var setup1 = new BikeSetup(simplyBike.BikeId, brakes1.PartId, "dobre hamulce");
            var setup2 = new BikeSetup(simplyBike.BikeId, frame1.PartId, "oryginalna rama 29");

            _context.BikeSetups.Add(setup1);
            _context.BikeSetups.Add(setup2);
            bikeBetter.BikeSetups.Add(setup1);
            bikeBetter.BikeSetups.Add(setup2);
            var ages = bikeBetter.AddBikeAges(2000, 1999);
            var ages2 = simplyBike.AddBikeAges(2005);

            _context.BikeAges.Add(ages);
            _context.BikeAges.Add(ages2);
            _context.Update(bikeBetter);
            brakes1.BikeSetups.Add(setup1);
            frame1.BikeSetups.Add(setup2);
            _context.SaveChanges();

            var bikeService = new Service(bikeBetter);
            _context.BikeServices.Add(bikeService);
            var bikeService2 = new Service(bikeBetter);
            _context.BikeServices.Add(bikeService2);

            _context.SaveChanges();

            var exampleDiagnosis = new Diagnosis("braking system", Grade.C, bikeService);
            var exampleDiagnosis2 = new Diagnosis("steering", Grade.B, bikeService);
            var exampleDiagnosis3 = new Diagnosis("steeringREMOVE-ME", Grade.B, bikeService);

            bikeService.RemoveDiagnosis(exampleDiagnosis3);

            Console.WriteLine("---");
        }

        static void MP1Examples(Bike exampleBike) {
            Console.WriteLine("==name before and after change==");
            Console.WriteLine(exampleBike.Name);
            exampleBike.Name = "testName";
            Console.WriteLine(exampleBike.Name);

            Console.WriteLine("\n==exception when willing to set null name==");
            try {
                exampleBike.Name = null;
            } catch (Exception) {
                Console.WriteLine("exception caught");
            }

            Console.WriteLine("\n==class method (getter) and attribute==");
            Console.WriteLine("Minimal number of gears: " + Bike.MinimalNumberOfGears);

            Console.WriteLine("\n==derived attribute==");
            Console.WriteLine("Bike age:" + exampleBike.BikeAge.Age);

            Console.WriteLine("\n==optional attribute before and after set==");
            Console.WriteLine("Additional info:" + exampleBike.AdditionalInfo);
            exampleBike.AdditionalInfo = "something";
            Console.WriteLine("Additional info:" + exampleBike.AdditionalInfo);

            Console.WriteLine("\n==Multi-value attribute==\nTeeth on racks:");
            foreach (var teeth in exampleBike.TeethsOnRacks) {
                Console.Write(teeth + ", ");
            }
        }
    }
}
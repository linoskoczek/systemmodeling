using MAS.Models;
using Microsoft.EntityFrameworkCore;

namespace MAS {
    public class BikeContext : DbContext {
        public DbSet<Bike> Bikes { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<BikeAge> BikeAges { get; set; }
        public DbSet<BikeSetup> BikeSetups { get; set; }
        public DbSet<Service> BikeServices { get; set; }
        public DbSet<Diagnosis> Diagnosis { get; set; }

        private static BikeContext _context { get; set; }
        public static BikeContext Context {
            get {
                if (_context is null) {
                    _context = new BikeContext();
                }
                return _context;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlite("Data Source=Bike.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<BikeSetup>()
                .HasKey(c => new { c.BikeId, c.PartId });

            modelBuilder.Entity<Bike>().HasKey(q => q.BikeId);
            modelBuilder.Entity<Part>().HasKey(q => q.PartId);
            modelBuilder.Entity<BikeSetup>().HasKey(q => new { q.BikeId, q.PartId });

        }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using MAS.Models;

namespace MAS {
    public class Service {
        private BikeContext _context = new BikeContext();
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServiceId { get; set; }
        private DateTime date;
        public DateTime Date {
            get => date;
            set {
                date = value;
            }
        }
        public int BikeId { get; private set; }
        public Bike Bike { get; private set; }

        [ForeignKey("Diagnosis")]
        public ICollection<Diagnosis> Diagnosis { get; set; }
        private Service() { }

        public Service(Bike bike) {
            Bike = bike;
            BikeId = bike.BikeId;
            Date = DateTime.Now;
        }

        ~Service() {
            foreach (var singleDiagnosis in Diagnosis) {
                singleDiagnosis.Dispose();
            }
        }

        public List<Diagnosis> GetDiagnosis() {
            return _context.Diagnosis.Where(d => d.ServiceId == this.ServiceId).ToList();
        }

        public void AddDiagnosis(Diagnosis diagnosis) {
            if (diagnosisAlreadyUsed(diagnosis)) {
                throw new ArgumentException("This diagnosis is already used!");
            }
            if (!containsDiagnosis(diagnosis)) {
                _context.SaveChanges();
            }
        }

        private bool containsDiagnosis(Diagnosis diagnosis) {
            return _context.Diagnosis.FirstOrDefault(d => d.DiagnosisId == diagnosis.DiagnosisId) != null;
        }

        private bool diagnosisAlreadyUsed(Diagnosis diagnosis) {
            return diagnosis.Service != null;
        }

        public void RemoveDiagnosis(Diagnosis diagnosis) {
            _context.Diagnosis.Remove(diagnosis);
            diagnosis.Dispose();
        }
    }
}
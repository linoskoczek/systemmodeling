using System;
using System.Collections.Generic;

namespace MAS.Models {
    public class Producer {
        private static Dictionary<string, Producer> producers = new Dictionary<string, Producer>();
        public static Dictionary<string, Producer> Producers {
            get => new Dictionary<string, Producer>(producers);
        }
        private string name;
        public string Name {
            get => name;
            set => name = value ??
                throw new ArgumentNullException();
        }
        public string Headquarters { get; private set; }

        private Producer() { }

        public Producer(string name, string headquarters) {
            if (!Producers.ContainsKey(name)) {
                this.Name = name ??
                    throw new ArgumentNullException();
                this.Headquarters = headquarters ??
                    throw new ArgumentNullException();
                producers.Add(name, this);
            }
        }

        public static Producer GetByName(string name) {
            return producers[name]; //throws exception if not found
        }

        public static bool Exists(string producerName) {
            return producers.ContainsKey(producerName);
        }
    }
}
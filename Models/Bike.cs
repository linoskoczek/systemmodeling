using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MAS.Models {
    public class Bike {
        public static short MinimalNumberOfGears { get; } = 1;
        public static List<Bike> GetBikesOlderThanYears(int years) {
            return BikeContext.Context.Bikes.Where(b => b.BikeAge != null && b.BikeAge.ProductionYear >= DateTime.Now.Year - years).ToList();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BikeId { get; private set; }
        private string name;
        public string Name {
            get => name;
            set => name = value ??
                throw new ArgumentNullException();
        }
        public virtual ICollection<BikeSetup> BikeSetups { get; set; }
        public virtual ICollection<Service> Services { get; set; }
        public string AdditionalInfo { get; set; }
        private BikeAge bikeAge;
        public BikeAge BikeAge {
            get => bikeAge;
            set {
                bikeAge = value ??
                    throw new ArgumentNullException();
            }
        }

        private IList<string> teethsOnRacks = new List<string>();
        public IList<string> TeethsOnRacks {
            get {
                return new List<string>(teethsOnRacks);
            }
        }

        [Required]
        public string TeethStr {
            get { return String.Join(',', teethsOnRacks); }
            set {
                teethsOnRacks = value.Split(',').ToList();
            }
        }

        public void AddTeeth(string teeth) {
            if (teeth == null || teethsOnRacks.Contains(teeth)) return;
            teethsOnRacks.Add(teeth);
        }

        public void RemoveTeeth(string teeth) {
            teethsOnRacks.Remove(teeth);
        }

        public Bike(string Name) {
            this.Name = Name;
            BikeSetups = new List<BikeSetup>();
        }

        public void LoadBikeData() {
            var context = new BikeContext();
            this.BikeAge = context.BikeAges.FirstOrDefault(a => a.BikeId == this.BikeId);
            this.BikeSetups = context.BikeSetups.Where(s => s.BikeId == this.BikeId).ToList();
            context.Dispose();
        }

        public BikeAge AddBikeAges(short ProductionYear, short DesignYear) {
            var age = new BikeAge(this.BikeId, ProductionYear, DesignYear);
            this.BikeAge = age;
            return age;
        }
        public BikeAge AddBikeAges(short ProductionYear) {
            return AddBikeAges(ProductionYear, ProductionYear);
        }

        public override string ToString() {
            string parts = "";

            foreach (var setup in BikeSetups) {
                parts += setup.Part.Name + ", ";
            }

            if (BikeAge == null) {
                return String.Format("[\nId: {0}\nName: {{1}\nParts: {2}\n]\n", BikeId, Name, parts);
            } else {
                return String.Format("[\nId: {0}\nName: {1}\nProduction year: {2}\nDesign year: {3}\nParts: {4}\n]\n",
                    BikeId, Name, BikeAge.ProductionYear, BikeAge.DesignYear, parts);
            }
        }
    }
}
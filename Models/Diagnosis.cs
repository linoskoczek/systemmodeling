using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAS {
    public enum Grade {
        A,
        B,
        C
    }

    public class Diagnosis : IDisposable {
        private BikeContext _context = new BikeContext();
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DiagnosisId { get; set; }
        private Grade grade;
        public Grade Grade {
            get => grade;
            set {
                if (!Enum.IsDefined(typeof(Grade), value)) throw new ArgumentException("Value not present in an enum");
                this.grade = value;
            }
        }

        [ForeignKey("Service")]
        public virtual int ServiceId { get; private set; }
        private Service service;
        public Service Service {
            get => service;
        }
        private string description;
        public string Description {
            get => description;
            set {
                description = value ??
                    throw new ArgumentNullException();
            }
        }

        private Diagnosis() { }
        public Diagnosis(string description, Grade grade, Service service) {
            this.Description = description;
            this.Grade = grade;
            setService(service);
            _context.Diagnosis.Add(this);
            _context.SaveChanges();
        }

        private void setService(Service service) {
            if (service == null) throw new ArgumentNullException();
            service.AddDiagnosis(this);
            this.service = service;
            this.ServiceId = service.ServiceId;
        }
        public void Dispose() {
            Console.WriteLine("Diagnosis goes away");
            _context.Diagnosis.Remove(this);
            _context.SaveChanges();
        }
    }
}
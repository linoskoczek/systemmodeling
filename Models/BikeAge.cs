using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MAS.Models {
    public class BikeAge {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BikeAgeId { get; private set; }
        public int BikeId { get; private set; }
        public Bike Bike { get; private set; }
        public int ProductionYear { get; private set; }
        public int DesignYear { get; private set; }
        public int Age {
            get => DateTime.Now.Year - ProductionYear;
        }
        public BikeAge(int BikeId, int ProductionYear, int DesignYear) {
            if (BikeId < 0 || ProductionYear < 1800 || DesignYear < 1800) throw new ArgumentException();
            var _context = new BikeContext();
            this.BikeId = BikeId;
            this.Bike = _context.Bikes.First(b => b.BikeId == BikeId);
            this.ProductionYear = ProductionYear;
            this.DesignYear = DesignYear;
            _context.Dispose();
        }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAS.Models {
    public class Part {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PartId { get; private set; }
        private string name;
        public string Name {
            get => name;
            set {
                if (value == null) throw new ArgumentNullException();
                else name = value;
            }
        }
        private string producerName { get; set; }
        public string ProducerName {
            get => producerName;
            set {
                if (Producer.Exists(value)) {
                    this.producerName = value;
                } else {
                    throw new ArgumentException("Producer does not exist!");
                }
            }
        }
        public ICollection<BikeSetup> BikeSetups { get; set; }

        private Part() { }

        public Part(string name, string producer) {
            this.Name = name;
            ProducerName = producer;
        }
    }
}
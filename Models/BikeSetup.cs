using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using MAS.Models;

namespace MAS {
    public class BikeSetup {
        public string SetupName { get; private set; }
        public int PartId { get; private set; }
        public Part Part { get; private set; }
        public int BikeId { get; private set; }
        public Bike Bike { get; private set; }

        public BikeSetup(int BikeId, int PartId, string SetupName) {
            if (PartId < 0 || BikeId < 0) throw new ArgumentNullException("PartId or BikeId wrong");
            this.SetupName = SetupName ?? throw new ArgumentNullException("Setup name cannot be null");
            this.PartId = PartId;
            this.BikeId = BikeId;
            var _context = new BikeContext();
            this.Part = _context.Parts.First(p => p.PartId == PartId);
            this.Bike = _context.Bikes.First(p => p.BikeId == BikeId);
        }
    }
}